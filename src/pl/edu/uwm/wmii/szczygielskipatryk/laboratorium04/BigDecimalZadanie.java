package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium04;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BigDecimalZadanie {
    public static void main(String[]args)
    {
        try {
           BigDecimal kapitalPoczatkowy = new BigDecimal(args[0]);
           BigDecimal stopaOprocentowania = new BigDecimal(args[1]);
           int okresOszczedzania = Integer.parseInt(args[2]);
           stopaOprocentowania=stopaOprocentowania.divide(BigDecimal.valueOf(10),RoundingMode.HALF_EVEN);
           BigDecimal wynik = new BigDecimal("0");
           for (int i = 0; i < okresOszczedzania; i++)
           {
               kapitalPoczatkowy=kapitalPoczatkowy.add(kapitalPoczatkowy.multiply(stopaOprocentowania));
               wynik=kapitalPoczatkowy;
           }
           wynik=wynik.setScale(2,RoundingMode.HALF_EVEN);
           System.out.println(wynik);
       }
       catch (ArrayIndexOutOfBoundsException e)
       {
           System.err.println("Zła liczba argumentów");
       }
    }
}
