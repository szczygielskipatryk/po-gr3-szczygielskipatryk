package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium04;

import java.math.BigInteger;

public class BigIntegerZadanie {
    public static void main(String[]args)
    {
        try {
            int help = Integer.parseInt(args[0]);
            int ilePol = help * help;
            BigInteger wynik = new BigInteger("0");
            int pomoc = 0;
            BigInteger tmp1 = new BigInteger("1");
            BigInteger mnoznik = new BigInteger("2");
            while (pomoc != ilePol) {
                BigInteger tmp2 = new BigInteger("1");
                tmp2 = tmp1.multiply(mnoznik);
                wynik = wynik.add(tmp1);
                tmp1 = tmp2;
                pomoc++;
            }
            System.out.println(wynik.toString());
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            System.err.println("Zła liczba argumentów");
        }
}
}
