package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium04;
import java.util.*;
public class Napisy {
    static int countChar(String str, char c) {
        int wynik = 0;
        char[] jacek = str.toCharArray();
        for (int i = 0; i < jacek.length; i++) {
            if (jacek[i] == c) {
                wynik++;
            }
        }
        return wynik;
    }

    static int countSubStr(String str, String subStr) {
        int wynik = 0;
        int id = 0;
        while ((id = str.indexOf(subStr, id)) != -1) {
            id++;
            wynik++;
        }
        return wynik;
    }

    static String middle(String str) {
        String wynik = "";
        char[] wer = str.toCharArray();
        char tmp = wer[wer.length / 2];
        wynik = Character.toString(tmp);
        if (wer.length % 2 == 0) {
            char tmp_2 = wer[(wer.length / 2) - 1];
            wynik = Character.toString(tmp_2) + Character.toString(tmp);
        }
        return wynik;
    }

    static String repeat(String str, int n) {
        String wynik = "";
        for (int i = 0; i < n; i++) {
            wynik = wynik + str;
        }
        return wynik;
    }

    static int[] where(String str, String subStr) {
        int ind = 0;
        int rozmiar = 0;
        int[] wynik = new int[rozmiar];
        for (int i = 0; i < str.length(); i++) {
            for (int j = i; j <= str.length(); j++) {
                if (str.substring(i, j).equals(subStr)) {
                    rozmiar++;
                    int[] tmp = new int[rozmiar];
                    for (int k = 0; k < wynik.length; k++) {
                        tmp[k] = wynik[k];
                    }
                    wynik = tmp;
                    wynik[ind] = i;
                    ind++;

                }
            }

        }
        System.out.println(Arrays.toString(wynik));
        return wynik;
    }

    static String change(String str) {
        char[] tmp = str.toCharArray();
        String wynik = "";
        StringBuffer ponka = new StringBuffer();
        for (int i = 0; i < tmp.length; i++) {
            if (Character.isUpperCase(tmp[i])) {
                tmp[i] = Character.toLowerCase(tmp[i]);
            } else {
                tmp[i] = Character.toUpperCase(tmp[i]);
            }
            ponka.append(tmp[i]);
        }
        wynik = ponka.toString();
        return wynik;
    }

    static String nice(String str) {
        String wynik = "";
        StringBuffer nowy=new StringBuffer();
        int ile=0;
        for(int i=str.length()-1;i>=0;i--)
        {
            char tmp=str.charAt(i);
            nowy.append(tmp);
            ile++;
            if(ile%3==0)
            {
                nowy.append('\'');
            }
        }
        nowy.reverse();
        wynik=nowy.toString();
        return wynik;
    }
    static String nice(String str, char c,int coIle) {
        String wynik = "";
        StringBuffer nowy=new StringBuffer();
        int ile=0;
        for(int i=str.length()-1;i>=0;i--)
        {
            char tmp=str.charAt(i);
            nowy.append(tmp);
            ile++;
            if(ile%coIle==0)
            {
                nowy.append(c);
            }
        }
        nowy.reverse();
        wynik=nowy.toString();
        return wynik;
    }



        public static void main (String[]args)
        {
            String jajcarz = "jajcarz";
            char a = 'a';
            System.out.println("Słowo "+jajcarz+" zawiera "+countChar(jajcarz,a)+" liter "+a);
            System.out.println(countSubStr("jajajajajaje","ja"));
            System.out.println(middle("hulajnogar"));
            Scanner in=new Scanner(System.in);
            String pobrana=in.next();
            System.out.println(repeat(pobrana,3));
            where("rabarbar", "bar");
            System.out.println(change("MeDuZaA"));
            System.out.println(nice("099567878979",'-',8));

        }
    }
