package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium04;
import java.io.*;
import java.util.*;

public class NapisyZadanie2 {
    public static void main(String []args)
    {
        if (args.length!=2)
        {
            System.err.println("Zła ilość argumentów");
            System.exit(1);
        }

        File cos = new File(args[0]);
        int ileWystapien=0;
        try {
            Scanner wczyt = new Scanner(cos);
            while (wczyt.hasNextLine()) {
                String str = wczyt.nextLine();
                char znak = args[1].charAt(0);
                ileWystapien += Napisy.countChar(str, znak);
            }
            wczyt.close();
        }
        catch(FileNotFoundException e)
        {
            System.err.println(cos.getPath()+" nie istnieje");
        }
        System.out.println(ileWystapien);
    }
}
