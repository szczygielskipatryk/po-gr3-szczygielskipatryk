package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium01;
import javax.lang.model.type.NullType;
import java.util.*;
class dzialania2
{
    public static Scanner skan=new Scanner(System.in);
    static int nieparzystość(int n)
{
    int wynik=0;
    for(int i=0;i<n;i++)
    {
        System.out.printf("Podaj %d wyraz ciągu: %n",i+1);
        int pobrane=skan.nextInt();
        if(pobrane%2!=0)
        {
            wynik++;
        }
    }
    return wynik;
}
    static int przez3nie5(int n)
    {
        int wynik=0;
        for(int i=0;i<n;i++)
        {
            System.out.printf("Podaj %d wyraz ciągu: %n",i+1);
            int pobrane=skan.nextInt();
            if(pobrane%3==0&&pobrane%5!=0)
            {
                wynik++;
            }
        }
        return wynik;
    }
    static int kwadratparzystej(int n)
    {
        int wynik=0;
        for(int i=0;i<n;i++)
        {
            System.out.printf("Podaj %d wyraz ciągu: %n",i+1);
            int pobrane=skan.nextInt();
            if(Math.sqrt(pobrane)%2==0)
            {
                wynik++;
            }
        }
        return wynik;
    }
    static int kplusminus(int n)
    {
        int [] tablicapomoc=new int[n];
        int wynik=0;
        for(int i=0;i<tablicapomoc.length;i++)
        {
            System.out.printf("Podaj %d wyraz ciągu: %n",i+1);
            tablicapomoc[i]=skan.nextInt();
        }
        for(int i=1;i<tablicapomoc.length-1;i++)
        {
            int przed=tablicapomoc[i-1];
            int po=tablicapomoc[i+1];
            int wlasciwa=tablicapomoc[i];
            if(wlasciwa<(przed+po)/2)
            {
                wynik++;
            }
        }
        return wynik;
    }
    static int kzpotega2(int n)
    {
        int wynik=0;

        for(int i=0;i<n;i++)
        {
            int pomoc=i+1;
            System.out.printf("Podaj %d wyraz ciągu: %n",pomoc);
            int wlasciwa=skan.nextInt();
            if(wlasciwa>Math.pow(2,pomoc)&&wlasciwa<dzialania.silnia(pomoc))
            {
                wynik++;
            }
        }
        return wynik;
    }
    static int parzysta_nieparzysta(int n) {
        int wynik = 0;

        for (int i = 1; i <= n; i++) {
            int pomoc = i;
            System.out.printf("Podaj %d wyraz ciągu: %n", pomoc);
            int wlasciwa = skan.nextInt();
            if (pomoc % 2 != 0 && wlasciwa % 2 == 0) {
                wynik++;
            }
        }
        return wynik;
    }
    static int nieparzysta_nieujemna(int n)
    {
        int wynik=0;

        for(int i=1;i<=n;i++)
        {
            System.out.printf("Podaj %d wyraz ciągu: %n",i+1);
            int wlasciwa=skan.nextInt();
            if(wlasciwa>=0&&wlasciwa%2!=0)
            {
                wynik++;
            }
        }
        return wynik;

}
    static int absodpotegi(int n)
    {
        int wynik = 0;

        for (int i = 1; i <= n; i++) {
            int pomoc = i;
            System.out.printf("Podaj %d wyraz ciągu: %n", pomoc);
            int wlasciwa = skan.nextInt();
            if (Math.abs(wlasciwa)<Math.pow(pomoc,2))
            {
                wynik++;
            }
        }
        return wynik;
    }
    static int zadanie2_2(int n)
    {
        int [] lista=new int[n];
        int wynik=0;
        for (int i=0;i<lista.length;i++)
        {
            System.out.printf("Podaj %d wyraz ciągu: %n", i+1);
            int element = dzialania2.skan.nextInt();
            lista[i]=element;
            if(lista[i]>0)
            {
                wynik+=lista[i];
            }
        }
        return wynik*2;
    }
    static String zadanie2_3(int n)
    {
        int [] lista=new int[n];
        int wynikujemne=0;
        int wynikdodatnie=0;
        int zera=0;
        for (int i=0;i<lista.length;i++)
        {
            System.out.printf("Podaj %d wyraz ciągu: %n", i+1);
            int element = dzialania2.skan.nextInt();
            lista[i]=element;
            if(lista[i]>0)
            {
                wynikdodatnie++;
            }
            else if(lista[i]<0)
            {
                wynikujemne++;
            }
            else
            {
                zera++;
            }

        }
        String wynik="Liczby dodatnie: "+wynikdodatnie+" Liczby ujemne: " +wynikujemne+ " zera: "+zera;
        return wynik;
    }
    static String zadanie2_4(int n)
    {

        int [] lista=new int[n];
        int maks=lista[0];
        int min=lista[0];
        for (int i=0;i<lista.length;i++)
        {
            System.out.printf("Podaj %d wyraz ciągu: %n", i+1);
            int element = dzialania2.skan.nextInt();
            lista[i]=element;
            if(lista[i]>maks)
            {
                maks=lista[i];
            }
            else if(lista[i]<min)
            {
                min=lista[i];
            }
        }
        String wynik="Największa liczba: "+maks+" najmniejsza liczba: "+min;
        return wynik;
    }
    static List<String> zadanie2_5(int n)
    {
        int a=0,b=0;
        int [] lista=new int[n];
        String wynik="";
        List<String>str=new ArrayList<>();
        for(int i=0;i<lista.length;i++)
        {
            System.out.printf("Podaj %d wyraz ciągu: %n", i+1);
            int element = dzialania2.skan.nextInt();
            lista[i]=element;
        }
        for(int i=1;i<lista.length;i++)
        {
            a=lista[i-1];
            b=lista[i];
            if(a>0&&b>0)
            {
                wynik="( "+a+","+b+" )";
                str.add(wynik);
            }
        }
        return str;
    }
    static void wyswielt(int n)
    {
        System.out.println(nieparzystość(n));
        System.out.println(przez3nie5(n));
        System.out.println(kwadratparzystej(n));
        System.out.println(kplusminus(n));
        System.out.println(kzpotega2(n));
        System.out.println(parzysta_nieparzysta(n));
        System.out.println(nieparzysta_nieujemna(n));
        System.out.println(absodpotegi(n));
        System.out.println(zadanie2_2(n));
        System.out.println(zadanie2_3(n));
        System.out.println(zadanie2_4(n));
        System.out.println(zadanie2_5(n));

    }
}

public class Zadanie2 {
    public static void main(String[] args)
    {
        System.out.println("Podaj n");
        int przykladowa = dzialania2.skan.nextInt();
       dzialania2.wyswielt(przykladowa);



    }
}
