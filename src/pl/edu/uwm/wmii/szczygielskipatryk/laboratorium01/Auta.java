package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium01;

import pl.edu.uwm.wmii.szczygielskipatryk.laboratorium02.Zadanie1_1;
import pl.edu.uwm.wmii.szczygielskipatryk.laboratorium02.Zadanie2_1;

import java.util.ArrayList;

class samochód
{
    String marka;
    int rocznik;
    float pojSilnika;
    float obliczSpalanieNaTrasie(int trasa)
    {
        float wynik=trasa*5*pojSilnika;
        return wynik;
    }
    public void PodajWartosci(String mark, int rok,float poje)
    {
        marka=mark;
        if(rok<=1900||rok>=2018)
        {
            System.out.println("Za stare te twoje auto");
        }
        else
        {
            rocznik=rok;
        }
        pojSilnika=poje;
    }
    public void wyswietl()
    {
        System.out.println("Marka auta to: " +marka);
        System.out.println("Rok Produkcji to: "+rocznik);
        System.out.println("Pojemność silnika to: "+pojSilnika);

    }



}

public class Auta
{
        public static void main(String[] args)
        {
            samochód auto1=new samochód();
            auto1.PodajWartosci("Renault",1998,(float)1.8);
            samochód auto2=new samochód();
            auto2.PodajWartosci("Opel",2004,(float)2.0);
            samochód auto3=new samochód();
            auto3.PodajWartosci("BMW",2007,(float)2.8);
            ArrayList<samochód> lista=new ArrayList<samochód>();
            lista.add(auto1);
            lista.add(auto2);
            lista.add(auto3);
            for(int i=0;i<lista.size();i++)
            {
                lista.get(i).wyswietl();
                System.out.println("Spalanie auta to: "+ lista.get(i).obliczSpalanieNaTrasie(30));
                System.out.println();
            }


        }

}
