package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium02;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Zadanie1_7 {
    public static void main(String[]args)
    {
        Scanner wczyt = new Scanner(System.in);
        int n = 0;
        System.out.println("Podaj rozmiar tablicy: ");
        int probka = wczyt.nextInt();
        if (probka >= 1 && probka <= 100) {
            n = probka;
        }
        int[] tablica = new int[n];
        Random gen=new Random();
        for (int i = 0; i < tablica.length; i++)
        {

            tablica[i] =gen.nextInt(1999)-999;

        }
        System.out.println(Arrays.toString(tablica));
        int lewy=wczyt.nextInt();
        int prawy=wczyt.nextInt();
        if(prawy>=1&&lewy>=1&&prawy<=tablica.length)
        {
            while (prawy > lewy)
            {
                int tmp = tablica[lewy-1];
                tablica[lewy-1] = tablica[prawy-1];
                tablica[prawy-1] = tmp;
                lewy++;
                prawy--;
            }
        }
        System.out.println(Arrays.toString(tablica));
    }
}
