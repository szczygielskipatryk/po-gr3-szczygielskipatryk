package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1_1 {
    public static void main(String[] args)
    {
        Scanner wczyt = new Scanner(System.in);
        int n = 0;
        System.out.println("Podaj rozmiar tablicy: ");
        int probka = wczyt.nextInt();
        if (probka >= 1 && probka <= 100) {
            n = probka;
        }
        int[] tablica = new int[n];
        Random generuj=new Random();
        for (int i = 0; i < tablica.length; i++)
        {

            tablica[i] =generuj.nextInt(1999)-999;

        }
        int wynik_parz = 0;
        int wynik_nieparz = 0;
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] % 2 == 0) {
                wynik_parz++;
            } else {
                wynik_nieparz++;
            }
        }
        String wynik = "Ilość liczb nieparzystych: " + wynik_nieparz + " ilość liczb parzystych: " + wynik_parz;
        System.out.println(wynik);
    }
}
