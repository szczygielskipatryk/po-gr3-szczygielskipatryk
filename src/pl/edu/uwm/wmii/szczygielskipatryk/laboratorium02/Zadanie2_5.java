package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium02;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Zadanie2_5
{
    public static int dlugoscMaksymalnegoCiaguDodatnich(int[] tablica)
    {
        int wynik = 0;
        List<Integer> pomoc = new ArrayList<>();
        pomoc.add(0);
        int maks = pomoc.get(0);
        for (int i = 0; i < tablica.length; i++)
        {
            if (tablica[i] > 0)
            {
                wynik++;
                if(i==tablica.length-1)
                {
                    pomoc.add(wynik);
                }
            }
            else if(tablica[i]<=0)
            {
                pomoc.add(wynik);
                wynik=0;
            }
        }
        for (int i = 0; i < pomoc.size(); i++) {
            if (maks < pomoc.get(i)) {
                maks = pomoc.get(i);
            }
        }
        return maks;
    }
    public static void main(String[] args)
    {
        Scanner wczyt = new Scanner(System.in);
        System.out.println("Podaj rozmiar tablicy(od 1 do 100): ");
        int n=wczyt.nextInt();
        int[] tablica=new int[n];
        System.out.println("Max: ");
        int max=wczyt.nextInt();
        System.out.println("Min: ");
        int min=wczyt.nextInt();
        Zadanie2_1.generuj(tablica,n,min,max);
        System.out.println(Arrays.toString(tablica));
        System.out.println(dlugoscMaksymalnegoCiaguDodatnich(tablica));
    }
}
