package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium02;
import java.util.*;

public class Zadanie3 {
   public static void main(String[] args)
   {
       Scanner wczyt=new Scanner(System.in);
       int m=wczyt.nextInt();
       int n=wczyt.nextInt();
       int k=wczyt.nextInt();
       Random rand=new Random();
       //[rzedy][kolumny]
       int[][] macierzA=new int[m][n];
       int[][] macierzB=new int[n][k];
       int[][] macierzC=new int[m][k];
       for(int i=0;i<macierzA.length;i++) {
           for (int j = 0; j < macierzA[0].length; j++) {
               macierzA[i][j] = rand.nextInt(10);
           }
       }
           for(int i=0;i<macierzB.length;i++)
           {
               for(int j=0;j<macierzB[0].length;j++)
               {
                   macierzB[i][j]=rand.nextInt(10);
               }
           }

       for(int i=0;i<m;i++) {
           for (int j = 0; j < k; j++) {
               for (int a = 0; a < n; a++)
               {
                   macierzC[i][j] += (macierzA[i][a] * macierzB[a][j]);
               }
           }
       }
       System.out.println("Macierz A");
       for(int i=0;i<macierzA.length;i++) {
           for (int j = 0; j < macierzA[0].length; j++) {
               System.out.print(macierzA[i][j] + " ");
           }
           System.out.println();
       }
System.out.println("Macierz B");
       for(int i=0;i<macierzB.length;i++) {
           for (int j = 0; j < macierzB[0].length; j++) {
               System.out.print(macierzB[i][j] + " ");
           }
           System.out.println();
       }
       System.out.println("Macierz C (wynik AxB)");
       for(int i=0;i<macierzC.length;i++)
       {
           for(int j=0;j<macierzC[0].length;j++)
           {
               System.out.print(macierzC[i][j]+" ");
           }
           System.out.println();
       }
   }
}
