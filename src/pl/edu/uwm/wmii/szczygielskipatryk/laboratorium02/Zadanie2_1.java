package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium02;
import java.util.*;

public class Zadanie2_1 {
    public static void generuj(int[] tab,int n ,int minWartosc, int maxWartosc)
    {
        Random r = new Random();
        if(n>=1&&n<=100)
        {
            if (minWartosc >= -999 && maxWartosc <= 999) {
                for (int i = 0; i < tab.length; i++) {
                    tab[i] = (r.nextInt((maxWartosc -minWartosc)) + minWartosc);
                }
            }
            else
            {
                System.out.println("Nieprawidłowy zakres liczb(min -999, max 999)");
                System.exit(1);
            }
        }
        else
        {
            System.out.println("Nieprawidłowy rozmiar tablicy(min 1, max 100)");
            System.exit(2);
        }
    }
    public static int ileNieparzystych(int[]tab)
    {
        int wynik=0;
        for(int i=0;i<tab.length;i++)
        {
            if(tab[i]%2!=0)
            {
                wynik++;
            }
        }
        return wynik;
    }
    public static int ileParzystych(int[]tab)
    {
        int wynik=0;
        for(int i=0;i<tab.length;i++)
        {
            if(tab[i]%2==0)
            {
                wynik++;
            }
        }
        return wynik;
    }
    public static void main(String[] args)
    {

    Scanner wczyt=new Scanner(System.in);
    System.out.println("Podaj rozmiar tablicy(od 1 do 100): ");
    int n=wczyt.nextInt();
    int[] tablica=new int[n];
    System.out.println("Max: ");
    int max=wczyt.nextInt();
    System.out.println("Min: ");
    int min=wczyt.nextInt();
    generuj(tablica,n,min,max);
    System.out.println(Arrays.toString(tablica));
    System.out.println("Ilość nieparzysztych: "+ileNieparzystych(tablica));
    System.out.println("Ilość parzystych: "+ileParzystych(tablica));
    }
}
