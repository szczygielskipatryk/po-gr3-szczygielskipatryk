package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium02;

import java.util.Arrays;
import java.util.Scanner;

public class Zadanie2_6 {
    public static void sigmum(int[] tablica)
    {
        for(int i=0;i<tablica.length;i++)
        {
            if(tablica[i]<0)
            {
                tablica[i]=-1;
            }
            else if(tablica[i]>0)
            {
                tablica[i]=1;
            }
        }
    }
    public static void main(String[] args)
    {
        Scanner wczyt = new Scanner(System.in);
        System.out.println("Podaj rozmiar tablicy(od 1 do 100): ");
        int n=wczyt.nextInt();
        int[] tablica=new int[n];
        System.out.println("Max: ");
        int max=wczyt.nextInt();
        System.out.println("Min: ");
        int min=wczyt.nextInt();
        Zadanie2_1.generuj(tablica,n,min,max);
        System.out.println(Arrays.toString(tablica));
        sigmum(tablica);
        System.out.println(Arrays.toString(tablica));

    }
}
