package pl.edu.uwm.wmii.kotewa.laboratorium00;

public class Zadanie4 {
    public static void main(String[] args)
    {
        double saldo_start=100.00;
        double saldo_po1roku=saldo_start+(saldo_start*0.06);
        System.out.println(saldo_po1roku);
        double saldo_po2latach=saldo_po1roku+(saldo_po1roku*0.06);
        System.out.println(saldo_po2latach);
        double saldo_po3latach=saldo_po2latach+(saldo_po2latach*0.06);
        System.out.format("%.2f%n", saldo_po3latach);
    }
}
