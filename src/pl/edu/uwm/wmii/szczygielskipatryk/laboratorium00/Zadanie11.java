package pl.edu.uwm.wmii.kotewa.laboratorium00;

public class Zadanie11 {
    public static void main(String[] args)
    {
        System.out.println("\"Czegóż płaczesz? - staremu mówił czyżyk młody -");
        System.out.println("Masz teraz lepsze w klatce niż w polu wygody\".");
        System.out.println("\"Tyś w niej zrodzon - rzekł stary - przeto ci wybaczę;");
        System.out.println("Jam był wolny, dziś w klatce - i dlatego płaczę\".");
    }
}
