package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium06;

public class IntegerSetTest {
    public static void main(String[]args)
    {
        IntegerSet test1=new IntegerSet();
        IntegerSet test2=new IntegerSet();
        test1.insertElement(3);
        test2.insertElement(3);
        test2.insertElement(1);

        IntegerSet dodaj=IntegerSet.union(test1,test2);
        test1.insertElement(1);
        IntegerSet wspolna=IntegerSet.intersection(test1,test2);
        System.out.println(dodaj+"\n"+ wspolna);
        System.out.println(test1.equals(test2));
        test2.deleteElement(1);
        System.out.println(test1);
        System.out.println(test1.equals(test2));
    }
}
