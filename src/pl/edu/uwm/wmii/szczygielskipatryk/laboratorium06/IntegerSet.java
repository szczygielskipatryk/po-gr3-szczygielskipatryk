package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium06;

import java.util.Arrays;

public class IntegerSet {

    public static IntegerSet union(IntegerSet pierwszy, IntegerSet drugi)
    {
        IntegerSet union=new IntegerSet();
        System.arraycopy(pierwszy.tablica,0,union.tablica,0,MAKSYMALNY_ROZMIAR);
        for(int i=0;i<MAKSYMALNY_ROZMIAR;i++)
        {
            if(drugi.tablica[i])
            {
                union.tablica[i]=true;
            }
        }
        return union;
    }
    public static IntegerSet intersection(IntegerSet pierwszy, IntegerSet drugi)
    {
        IntegerSet intersection=new IntegerSet();
        for(int i=0;i<MAKSYMALNY_ROZMIAR;i++)
        {
            intersection.tablica[i]=pierwszy.tablica[i]&&drugi.tablica[i];
        }
        return intersection;
    }
    public void insertElement(int liczba) {
        try {
            this.tablica[liczba - 1] = true;
        }
        catch (Exception e)
        {
            System.err.println("Przekroczono rozmiar tablicy");
        }
    }
    public void deleteElement(int liczba)
    {
        this.tablica[liczba-1]=false;
    }

    @Override
    public String toString() {
        return Arrays.toString(tablica);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        IntegerSet that = (IntegerSet) o;
        return Arrays.equals(tablica, that.tablica);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(tablica);
    }

    private static final int MAKSYMALNY_ROZMIAR=100;
    private final boolean[] tablica=new boolean[MAKSYMALNY_ROZMIAR];
}
