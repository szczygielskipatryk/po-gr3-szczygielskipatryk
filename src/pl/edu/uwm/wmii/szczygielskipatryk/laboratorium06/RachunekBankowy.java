package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium06;

public class RachunekBankowy {
    public RachunekBankowy(double saldo) {
        this.saldo = saldo;
    }

    public void setRocznaStopaProcentowa(double rocznaStopaProcentowa) {
        this.rocznaStopaProcentowa = rocznaStopaProcentowa/100;
    }
    public void obliczMiesieczneOdsetki()
    {
        double tmp=(saldo*rocznaStopaProcentowa)/12;
        this.saldo+=tmp;
        System.out.format("%.2f%n",saldo);
    }
    private double saldo;
    double rocznaStopaProcentowa;
}
