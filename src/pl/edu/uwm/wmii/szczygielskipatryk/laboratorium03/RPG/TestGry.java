package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium03.RPG;

public class TestGry {
    public static void main(String[] args) {
        Wojownik Aragorn = new Wojownik("Aragorn", 1259, 15, 1);
        System.out.println(Aragorn);
        Lucznik Legolas = new Lucznik("Legolas", 874, 13, 3);
        System.out.println(Legolas);
        Aragorn.obliczmocataku();
        System.out.println(Aragorn.pomocnicza);
        Aragorn.utratahp(Legolas);
        System.out.println(Aragorn.pomocnicza);
        Aragorn.utratahp(1259);
        System.out.println(Aragorn);
    }
}
