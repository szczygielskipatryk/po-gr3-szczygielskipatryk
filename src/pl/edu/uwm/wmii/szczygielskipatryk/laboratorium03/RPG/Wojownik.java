package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium03.RPG;

public class Wojownik extends Bohater {

    public Wojownik(String imie, int hp, int sila, int PT) {
        this.HP = hp;
        this.Name = imie;
        this.ability = sila;
        this.ST = PT;
    }

    @Override void  obliczmocataku() {
        if (this.HP > 0) {
            this.mocataku = (int) (this.ability * this.ST * this.pomocnicza/10);
            {
                if (this.pomocnicza < 20) {
                    this.mocataku = this.ability * this.ST * 150;
                }
            }
        }

    }

}
