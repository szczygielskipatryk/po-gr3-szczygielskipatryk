package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium03.Bank;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

class Konto {
    public Konto(double stan_poczatkowy, String nazwa, String bank) {
        this.stan_poczatkowy = stan_poczatkowy;
        this.nazwa = nazwa;
        this.bank = bank;
    }
    public void wyplata_bankomat(double ile,int bankomat) throws IOException {
        String plik=(this.nazwa+"_log.txt");
        double tmp=this.stan_poczatkowy-ile;
        double tmp_2=this.stan_poczatkowy;
        this.stan_poczatkowy=tmp;
        String nowa=Double.toString(ile);
        String wynik=("Wypłata z bankomatu numer: "+bankomat+"\nStan przed operacją "+ tmp_2+"zł\nWypłacono "+ nowa+ "zł  \nData:"+timeStamp+"\n\n");
        FileWriter log=new FileWriter(plik,true);
        log.write(wynik);
        log.close();
    }
    public void stan_konta(int bankomat) throws IOException
    {
        String plik=(this.nazwa+"_log.txt");

        System.out.println(this.stan_poczatkowy);
        String wyjscie=("Sprawdzono stan konta w bankomacie nr:"+bankomat+"\nSaldo: "+ this.stan_poczatkowy+"\nData:"+timeStamp+"\n\n");
        FileWriter log=new FileWriter(plik,true);
        log.write(wyjscie);
        log.close();
    }
    public void stan_konta() throws IOException
    {
        String plik=(this.nazwa+"_log.txt");

        System.out.println(this.stan_poczatkowy);
        String wyjscie=("Sprawdzono stan konta online\nSaldo: "+ this.stan_poczatkowy+"\nData:"+timeStamp+"\n\n");
        FileWriter log=new FileWriter(plik,true);
        log.write(wyjscie);
        log.close();
    }
    public void zaplata_karta(String gdzie, double ile) throws IOException
    {
        String plik=(this.nazwa+"_log.txt");
        double tmp=this.stan_poczatkowy-ile;
        double tmp_2=this.stan_poczatkowy;
        this.stan_poczatkowy=tmp;
        String nowa=Double.toString(ile);
        String wynik=("Zapłata kartą\nNazwa sprzedawcy:" +gdzie+"\nStan przed operacją "+ tmp_2+"zł\nZaplacono "+ nowa+ "zł  \nData:"+timeStamp+"\n\n");
        FileWriter log=new FileWriter(plik,true);
        log.write(wynik);
        log.close();

    }
    public void przelew(Konto doKogo,double ile,String tytul) throws IOException
    {
        String plik=(this.nazwa+"_log.txt");
        String plik2=(doKogo.nazwa+"_log.txt");
        double tmp=this.stan_poczatkowy-ile;
        doKogo.stan_poczatkowy=doKogo.stan_poczatkowy+ile;
        double tmp_2=this.stan_poczatkowy;
        this.stan_poczatkowy=tmp;
        String nowa=Double.toString(ile);
        String wynik=("Przelew\nOdbiorca:" +doKogo.nazwa+"\nZ tytułem:\""+tytul+"\" \nStan przed operacją "+ tmp_2+"zł\nPrzelano "+ nowa+ "zł  \nData:"+timeStamp+"\n\n");
        FileWriter log=new FileWriter(plik,true);
        log.write(wynik);
        log.close();
        double tmp_3=doKogo.stan_poczatkowy-ile;
        String nowa2=Double.toString(tmp_3);
        String wynik2=("Przelew\nNadawca:" +this.nazwa+"\nZ tytułem: \""+ tytul+"\"\nStan przed operacją "+nowa2 +"zł\nPrzelano "+ nowa+ "zł  \nData:"+timeStamp+"\n\n");
        FileWriter log2=new FileWriter(plik2,true);
        log2.write(wynik2);
        log2.close();
    }
    public void wyplata_kasa(String nazwaOddzialu,double ile) throws IOException
    {
        String plik=(this.nazwa+"_log.txt");
        double tmp=this.stan_poczatkowy-ile;
        double tmp_2=this.stan_poczatkowy;
        this.stan_poczatkowy=tmp;
        String nowa=Double.toString(ile);
        String wynik=("Wypłata w kasie: "+nazwaOddzialu+"\nStan przed operacją "+ tmp_2+"zł\nWypłacono "+ nowa+ "zł  \nData:"+timeStamp+"\n\n");
        FileWriter log=new FileWriter(plik,true);
        log.write(wynik);
        log.close();
    }
    protected String timeStamp = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(new Date());
    protected double stan_poczatkowy;
    protected String nazwa;
    protected String bank;
}
