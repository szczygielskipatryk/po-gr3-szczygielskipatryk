package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium03.Bank;

import java.io.FileWriter;
import java.io.IOException;

public class KontoPrywatne extends Konto {

    public KontoPrywatne(double stan_poczatkowy, String nazwa, String bank) {
        super(stan_poczatkowy, nazwa, bank);
    }

    public void zapomoga(int ile, String instytucja) throws IOException
    {
        String plik=(this.nazwa+"_log.txt");
        double tmp=this.stan_poczatkowy+ile;
        double tmp_2=this.stan_poczatkowy;
        this.stan_poczatkowy=tmp;
        String nowa=Double.toString(ile);
        String wynik=("Otrzymano zapomoge\nInstytucja: "+instytucja+"\nWpłacono: "+ nowa+ "zł " + "\nStan po operacji: "+ tmp+"zł \nData:"+timeStamp+"\n\n");
        FileWriter log=new FileWriter(plik,true);
        log.write(wynik);
        log.close();
    }


}

