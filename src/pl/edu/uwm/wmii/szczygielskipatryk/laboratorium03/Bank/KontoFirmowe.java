package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium03.Bank;

import java.io.FileWriter;
import java.io.IOException;

public class KontoFirmowe extends Konto {

    public KontoFirmowe(double stan_poczatkowy, String nazwa, String bank) {
        super(stan_poczatkowy, nazwa, bank);
    }

    public void wplataZUS(String pracownik,double ile, String urzad)throws IOException
    {
        String plik=(this.nazwa+"_log.txt");
        double tmp=this.stan_poczatkowy-ile;
        this.stan_poczatkowy=tmp;
        String nowa=Double.toString(ile);
        String wynik=("Zaplacono składke ZUS\nDo urzędu "+ urzad+"\nDla pracownika: "+pracownik+"\nWpłacono: "+ nowa+ "zł " + "\nStan po operacji: "+ tmp+"zł \nData:"+timeStamp+"\n\n");
        FileWriter log=new FileWriter(plik,true);
        log.write(wynik);
        log.close();
    }
    public void wplataUS(double ile, String urzad)throws IOException {
        String plik = (this.nazwa + "_log.txt");
        double tmp = this.stan_poczatkowy - ile;
        this.stan_poczatkowy = tmp;
        String nowa = Double.toString(ile);
        String wynik = ("Wpłata do urzędu skarbowego\nDo urzędu " + urzad + "\nWpłacono: " + nowa + "zł " + "\nStan po operacji: " + tmp + "zł \nData:" + timeStamp + "\n\n");
        FileWriter log = new FileWriter(plik, true);
        log.write(wynik);
        log.close();
    }
    public void wyplata(int ile, Konto dlaKogo) throws IOException
    {
        String plik=(this.nazwa+"_log.txt");
        String plik2=(dlaKogo.nazwa+"_log.txt");
        double tmp=this.stan_poczatkowy-ile;
        double tmp_2=this.stan_poczatkowy;
        this.stan_poczatkowy=tmp;
        double tmp_3=dlaKogo.stan_poczatkowy+ile;
        dlaKogo.stan_poczatkowy=dlaKogo.stan_poczatkowy+ile;
        String nowa=Double.toString(ile);
        String wynik=("Wypłacono wynagrodzenie\nDla pracownika "+ dlaKogo.nazwa +"\nWypłacono "+ nowa+ "zł " + "\nStan po operacji "+ tmp+"zł \nData:"+timeStamp+"\n\n");
        String wynik2=("Otrzymano wynagrodzenie\nOd firmy "+ this.nazwa +"\nWpłacono "+ nowa+ "zł " + "\nStan po operacji "+ tmp_3+"zł \nData:"+timeStamp+"\n\n");
        FileWriter log=new FileWriter(plik,true);
        log.write(wynik);
        log.close();
        FileWriter log2=new FileWriter(plik2,true);
        log2.write(wynik2);
        log2.close();
    }
}
