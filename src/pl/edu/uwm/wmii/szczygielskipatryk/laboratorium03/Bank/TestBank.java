package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium03.Bank;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

public class TestBank {
    public static void main(String[] args) throws IOException {
        KontoPrywatne cos=new KontoPrywatne(900,"Jan Kowalski","PKO");
        Random randomowa=new Random();
        int bankomat=randomowa.nextInt(100);
        //cos.wyplata_bankomat(20);
        cos.wyplata_bankomat(301, bankomat);
        cos.stan_konta(bankomat);
        cos.zaplata_karta("McDonald",20);
        cos.stan_konta();
        KontoPrywatne ktos=new KontoPrywatne(300,"Piotr Nowak","mBank");
        cos.przelew(ktos,30,"Na imieniny");
        ktos.stan_konta();
        cos.stan_konta();
        cos.wyplata_kasa("Kasa na rogu",50);
        cos.stan_konta(bankomat);
        ktos.zapomoga(200,"MOPS");
        KontoFirmowe zakis=new KontoFirmowe(100000,"Zakis","Pekao");
        zakis.wplataZUS(cos.nazwa,156,"ZUS w Sosnowcu");
        zakis.stan_konta();
        zakis.wplataUS(123.25,"Urząd skarbowy w Grójcu");
        zakis.wyplata(3000,cos);

    }
}
