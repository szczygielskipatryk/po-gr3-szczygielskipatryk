package pl.edu.uwm.wmii.szczygielskipatryk.laboratorium05;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class ArrayListZadanie3 {
    static ArrayList<Integer>reversed(ArrayList<Integer>a)
    {
        ArrayList<Integer>wynik=new ArrayList<>();
        for(int i=0;i<a.size();i++)
        {
            wynik.add(0);
        }
        for(int i=0;i<wynik.size();i++)
        {
            wynik.set(i,a.get(a.size()-1-i));
        }
        return wynik;
    }
    static void reverse(ArrayList<Integer>a)
    {
        int tmp=0;
        for(int i=0;i<(a.size()/2)-1;i++)
        {
            tmp=a.get(i);
            a.set(i,a.get(a.size()-1-i));
            a.set(a.size()-1-i,tmp);

        }
        System.out.println(a);
    }
    public static void main(String[]args)
    {
        ArrayList<Integer>a=new ArrayList<>(Arrays.asList(33,21,1,2,3,4,5,17,22,412));
        System.out.println(reversed(a));
        reverse(a);
    }
}
