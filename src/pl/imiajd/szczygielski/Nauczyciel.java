package pl.imiajd.szczygielski;

class Nauczyciel extends Osoba {
    public Nauczyciel(String nazw,int rok,int pensja)
    {
        super(nazw,rok);
        this.pensja=pensja;
    }
    public String toString()
    {
        String wynik=("Nauczyciel "+getNazwisko()+". Rok urodzenia "+getRokurodzena()+". Pensja "+ this.pensja+" zł");
        return wynik;
    }

    public int getPensja() {
        return pensja;
    }

    private int pensja;
}
