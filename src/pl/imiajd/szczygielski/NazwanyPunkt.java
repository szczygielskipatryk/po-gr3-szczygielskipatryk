package pl.imiajd.szczygielski;

public class NazwanyPunkt extends Punkt {
    NazwanyPunkt(int x, int y, String nazwa)
    {
        super(x,y);
        this.nazwa=nazwa;
    }
    public void show()
    {
        System.out.println("<"+ x()+", "+ y() +">");
    }
    private String nazwa;
}
