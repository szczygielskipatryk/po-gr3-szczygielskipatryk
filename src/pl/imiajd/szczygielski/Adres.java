package pl.imiajd.szczygielski;

class Adres {
    public Adres(int numer)
    {
        this.numer_mieszkania=numer;
    }
    public Adres()
    {

    }
    public void show()
    {
        System.out.println(kod_pocztowy+" "+miasto);
        System.out.println("ul."+ulica+" "+numer_domu+"/"+numer_mieszkania);
    }
    public boolean przed(Adres o)
    {
        if (o.kod_pocztowy<this.kod_pocztowy)
        {
            return true;
        }
        return false;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public void setNumer_domu(int numer_domu) {
        this.numer_domu = numer_domu;
    }

    public void setNumer_mieszkania(int numer_mieszkania) {
        this.numer_mieszkania = numer_mieszkania;
    }

    public void setMiasto(String miasto) {
        this.miasto = miasto;
    }

    public void setKod_pocztowy(int kod_pocztowy) {
        this.kod_pocztowy = kod_pocztowy;
    }

    private String ulica;
    private int numer_domu;
    private int numer_mieszkania;
    private String miasto;
    private int kod_pocztowy;
}
