package pl.imiajd.szczygielski;

abstract class Osoba {
public Osoba(String nazw,int rok)
{
    this.nazwisko=nazw;
    this.rokurodzena=rok;
}
public abstract String toString();

    public String getNazwisko() {
        return nazwisko;
    }

    public int getRokurodzena() {
        return rokurodzena;
    }

    private String nazwisko;
private int rokurodzena;
}
