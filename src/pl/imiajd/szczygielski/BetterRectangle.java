package pl.imiajd.szczygielski;

class BetterRectangle extends java.awt.Rectangle {
    public BetterRectangle(int x, int y, int a, int b)throws Exception
    {
        super(x,y,a,b);
        if(a<0||b<0)
        {
            throw new Exception();
        }
    }
    public double getPerimeter()
    {
        double wynik=2*getSize().height+2*getSize().width;
        return wynik;
    }
    public double getArea()
    {
        double wynik=getSize().height*getSize().width;
        return wynik;
    }
}
