package pl.imiajd.szczygielski;

public class AdresTest {
    public static void main(String[]args)
    {
        Adres pierwszy=new Adres();
        pierwszy.setKod_pocztowy(12200);
        pierwszy.setMiasto("Pisz");
        pierwszy.setUlica("Grunwaldzka");
        pierwszy.setNumer_domu(12);
        pierwszy.setNumer_mieszkania(31);
        pierwszy.show();
        Adres drugi=new Adres(2);
        drugi.setNumer_domu(4);
        drugi.setUlica("Ełcka");
        drugi.setKod_pocztowy(12250);
        drugi.setMiasto("Orzysz");
        System.out.println(pierwszy.przed(drugi));
        drugi.show();
    }
}
