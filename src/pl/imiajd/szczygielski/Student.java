package pl.imiajd.szczygielski;

class Student extends Osoba {
    public Student(String nazw,int rok,String kierunek)
    {
        super(nazw, rok);
        this.kierunek=kierunek;
    }

    public String toString()
    {
        String wynik=("Student "+getNazwisko()+". Rok urodzenia "+getRokurodzena()+". Kierunek studiów "+ this.kierunek);
        return wynik;
    }

    public String getKierunek() {
        return kierunek;
    }

    private String kierunek;
}
